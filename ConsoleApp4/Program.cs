﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using RestSharp;

namespace ConsoleApp4
{
    class Program
    {
        static ExchangeRate Init()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://api.privatbank.ua/p24api");
            //for some reason, PB does not have exchange rate for today
            var date = DateTime.Today.Date.AddDays(-1).ToShortDateString();
            var request = new RestRequest($"/exchange_rates?json=None&date={date}", Method.GET);
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<ExchangeRate>(response.Content);
        }

        static void WriteExchangeRates(ExchangeRate exchange)
        {
            Console.WriteLine($"Exchange rates for {exchange.baseCurrencyLit} on {exchange.date} date: \n");

            foreach (var rate in exchange.exchangeRate)
            {
                Console.WriteLine($"Currency: {rate.currency}, sale rate: {rate.saleRateNB}, purchase rate: {rate.purchaseRateNB}");
            }
        }

        static void Main(string[] args)
        {
            var exchange = Init();
            WriteExchangeRates(exchange);
            Console.ReadLine();
        }
    }

    public class ExchangeRate
    {
        public string date { get; set; }
        public string bank { get; set; }
        public string baseCurrency { get; set; }
        public string baseCurrencyLit { get; set; }
        public List<Exchange> exchangeRate { get; set; }
    }
    public class Exchange
    {
        public string baseCurrency { get; set; }
        public string currency { get; set; }
        public float saleRateNB { get; set; }
        public float purchaseRateNB { get; set; }
    }
}